package com.artem.springboottodo.db;

import com.artem.springboottodo.entity.TodoEntity;
import com.artem.springboottodo.repositories.TodoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadDatabase {
    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initTodoTaskDatabase(TodoRepository repository) {
        return args -> {
            log.info("Preloading " + repository.save(new TodoEntity("Create Picture", "need do something")));
            log.info("Preloading " + repository.save(new TodoEntity("Clean car", "need do something")));
            log.info("Preloading " + repository.save(new TodoEntity("Find a job", "need do something")));
            log.info("Preloading " + repository.save(new TodoEntity("Move to Vacation", "need do something")));
        };
    }

}
