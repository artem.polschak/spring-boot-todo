package com.artem.springboottodo.config;

import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class CustomLocaleResolver extends AcceptHeaderLocaleResolver implements WebMvcConfigurer {

    private static final Locale DEFAULT_LOCALE = Locale.US;
    private final List<Locale> locales = Arrays.asList(new Locale("en"), new Locale("uk"));

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        String headerLang = request.getHeader("Accept-Language");
        return headerLang == null || headerLang.isEmpty()
                ? DEFAULT_LOCALE
                : Locale.lookup(Locale.LanguageRange.parse(headerLang), locales);
    }
}
