package com.artem.springboottodo.models;

import com.artem.springboottodo.services.Status;

import javax.validation.constraints.Size;
import java.util.Objects;

public class TodoDTO {
    private String name;
    private String description;
    private Status status;

    @Size(min = 2, max = 40)
    public String getName() {
        return name;
    }

    public TodoDTO setName(String name) {
        this.name = name;
        return this;
    }

    @Size(min = 2, max = 300)
    public String getDescription() {
        return description;
    }

    public TodoDTO setDescription(String description) {
        this.description = description;
        return this;
    }


    public Status getStatus() {
        return status;
    }

    public TodoDTO setStatus(Status status) {
        this.status = status;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TodoDTO todoDTO = (TodoDTO) o;
        return Objects.equals(name, todoDTO.name) && Objects.equals(description, todoDTO.description) && status == todoDTO.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, status);
    }

    @Override
    public String toString() {
        return "TodoDTO{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", status=" + status +
                '}';
    }
}
