package com.artem.springboottodo.controllers;

import com.artem.springboottodo.exception.*;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ErrorHandlingControllerAdvice {

    private static final String MESSAGE = "message";

    @ResponseBody
    @ExceptionHandler(TodoBadRequestException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    Map<String, String> forbiddenHandler(TodoBadRequestException exception) {
        return Map.of(MESSAGE, exception.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((ObjectError error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    @ExceptionHandler(ErrorResponse .class)
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ResponseBody
    Map<String, String> handleCustomException(ErrorResponse  ex) {
        return Map.of(MESSAGE, ex.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(TodoNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    Map<String, String> notFoundHandler(TodoNotFoundException exception) {
        return Map.of(MESSAGE, exception.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(UnauthorizedActionException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    Map<String, String> handleUnauthorizedActionException(UnauthorizedActionException exception) {
        return Map.of(MESSAGE, exception.getMessage());
    }
}
