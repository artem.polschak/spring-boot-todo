package com.artem.springboottodo.controllers;

import com.artem.springboottodo.entity.TodoEntity;
import com.artem.springboottodo.models.StatusDTO;
import com.artem.springboottodo.models.TodoDTO;
import com.artem.springboottodo.services.TodoService;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@SecurityScheme(
        name = "Authorize",
        scheme = "Basic",
        type = SecuritySchemeType.HTTP,
        in = SecuritySchemeIn.HEADER)
@SecurityRequirement(name = "Authorize")
@RestController
@RequestMapping("/api/v4/tasks")
@Validated
public class TodoController {

    private final TodoService todoService;

    TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping("/")
    public ResponseEntity<Iterable<TodoEntity>> getAllTasks() {
        return ResponseEntity.status(HttpStatus.OK).body(todoService.readAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<TodoEntity> getTaskById(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(todoService.getById(id));
    }

    @PostMapping
    public ResponseEntity<TodoEntity> createTask(@Valid @RequestBody TodoDTO todoDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(todoService.create(todoDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<TodoEntity> updateTask(@PathVariable Long id, @RequestBody StatusDTO newStatus) {
        return ResponseEntity.status(HttpStatus.OK).body(todoService.updateStatus(id, newStatus));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTask(@PathVariable Long id) {
            todoService.deleteTask(id);
            return ResponseEntity.noContent().build();
    }
}
