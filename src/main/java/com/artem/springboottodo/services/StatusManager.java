package com.artem.springboottodo.services;

import com.artem.springboottodo.models.StatusDTO;
import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.Map;
import java.util.Set;

@Component
public class StatusManager {
    private final Map<Status, Set<Status>> allowedTransitions;

    public StatusManager() {
        this.allowedTransitions = new EnumMap<>(Status.class);
        allowedTransitions.put(Status.PLANNED, Set.of(Status.WORK_IN_PROGRESS, Status.POSTPONED, Status.CANCELLED));
        allowedTransitions.put(Status.WORK_IN_PROGRESS, Set.of(Status.NOTIFIED, Status.SIGNED, Status.CANCELLED));
        allowedTransitions.put(Status.POSTPONED, Set.of(Status.NOTIFIED, Status.SIGNED, Status.CANCELLED));
        allowedTransitions.put(Status.SIGNED, Set.of(Status.CANCELLED, Status.DONE));
        allowedTransitions.put(Status.NOTIFIED, Set.of(Status.CANCELLED, Status.DONE));
    }

    public boolean isAllowed(Status currentStatus, StatusDTO newStatus) {
        if (currentStatus == Status.CANCELLED || currentStatus == Status.DONE) {
            return false;
        }
        Set<Status> allowedNextStatuses = allowedTransitions.get(currentStatus);
        return allowedNextStatuses.contains(Status.valueOf(newStatus.getStatus()));
    }
}
