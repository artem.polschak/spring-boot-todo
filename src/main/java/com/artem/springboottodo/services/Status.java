package com.artem.springboottodo.services;

public enum Status {
    PLANNED,
    WORK_IN_PROGRESS,
    POSTPONED,
    NOTIFIED,
    SIGNED,
    CANCELLED,
    DONE
}