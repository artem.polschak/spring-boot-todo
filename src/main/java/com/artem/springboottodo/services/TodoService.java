package com.artem.springboottodo.services;

import com.artem.springboottodo.entity.TodoEntity;
import com.artem.springboottodo.exception.TodoBadRequestException;
import com.artem.springboottodo.exception.TodoNotFoundException;
import com.artem.springboottodo.models.StatusDTO;
import com.artem.springboottodo.models.TodoDTO;
import com.artem.springboottodo.repositories.TodoRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class TodoService {
    private static final Logger logger = LogManager.getLogger(TodoService.class);
    private final TodoRepository repository;
    private final MessageSource messageSource;
    private final StatusManager statusManager;

    public TodoService(TodoRepository repository, MessageSource messageSource, StatusManager statusManager) {
        this.repository = repository;
        this.messageSource = messageSource;
        this.statusManager = statusManager;
    }

    public TodoEntity create(TodoDTO todoDTO) {
        return repository.save(new TodoEntity()
                .setName(todoDTO.getName())
                .setDescription(todoDTO.getDescription())
                .setStatus(Status.PLANNED));
    }

    public Iterable<TodoEntity> readAll() {
        return repository.findAll();
    }

    public TodoEntity getById(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new TodoNotFoundException(
                        String.format(messageSource.getMessage(
                                "exception.notFoundId", null, LocaleContextHolder.getLocale()), id)));
    }

    public void deleteTask(Long id) {
        try {
            repository.deleteById(id);
            logger.info("Task with ID {} deleted", id);
        } catch (EmptyResultDataAccessException exception) {
            throw new TodoNotFoundException(String.format(messageSource.getMessage(
                    "exception.cannotDelete", null, LocaleContextHolder.getLocale()), id));
        }
    }

    public TodoEntity updateEntity(TodoEntity todoEntity, Long id) {
        TodoEntity entity = repository.findById(id)
                .orElseThrow(() -> new TodoNotFoundException(String.format(messageSource.getMessage(
                        "exception.notFoundId", null, LocaleContextHolder.getLocale()), id)));
        return repository.save(entity
                .setName(todoEntity.getName())
                .setDescription(todoEntity.getDescription())
                .setStatus(todoEntity.getStatus()));

    }

    public TodoEntity updateStatus(Long taskId, StatusDTO newStatus) {
        TodoEntity todo = getById(taskId);
        Status currentStatus = todo.getStatus();

        if (!statusManager.isAllowed(currentStatus, newStatus)) {
            throw new TodoBadRequestException(String.format(messageSource.getMessage(
                    "exception.invalidStatusTransaction", null, LocaleContextHolder.getLocale())));
        }

        todo.setStatus(Status.valueOf(newStatus.getStatus()));
        return updateEntity(todo, taskId);

    }
}
