package com.artem.springboottodo.services;

import com.artem.springboottodo.entity.TodoEntity;
import com.artem.springboottodo.exception.TodoBadRequestException;
import com.artem.springboottodo.exception.TodoNotFoundException;
import com.artem.springboottodo.models.StatusDTO;
import com.artem.springboottodo.models.TodoDTO;
import com.artem.springboottodo.repositories.TodoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TodoServiceTest {

    @Autowired
    private TodoService todoService;

    @Autowired
    private TodoRepository todoRepository;

    @BeforeEach
    public void setUp() {
        todoRepository.deleteAll();
    }

    @Test
    void testCreateTodo() {
        TodoDTO todoDTO = new TodoDTO()
                .setName("Test Task")
                .setDescription("Test Description");

        TodoEntity createdTodo = todoService.create(todoDTO);

        assertNotNull(createdTodo.getId());
        assertEquals("Test Task", createdTodo.getName());
        assertEquals("Test Description", createdTodo.getDescription());
        assertEquals(Status.PLANNED, createdTodo.getStatus());
    }

    @Test
    void testReadAllTodos() {
        TodoDTO todoDTO1 = new TodoDTO().setName("Task 1").setDescription("Description 1");
        TodoDTO todoDTO2 = new TodoDTO().setName("Task 2").setDescription("Description 2");

        todoService.create(todoDTO1);
        todoService.create(todoDTO2);

        Iterable<TodoEntity> todos = todoService.readAll();
        List<TodoEntity> todoList = new ArrayList<>();
        todos.forEach(todoList::add);

        assertEquals(2, todoList.size());
    }

    @Test
    void testGetTodoById() {
        TodoDTO todoDTO = new TodoDTO().setName("Task 1").setDescription("Description 1");
        TodoEntity createdTodo = todoService.create(todoDTO);

        Long id = createdTodo.getId();

        TodoEntity retrievedTodo = todoService.getById(id);

        assertNotNull(retrievedTodo);
        assertEquals("Task 1", retrievedTodo.getName());
        assertEquals("Description 1", retrievedTodo.getDescription());
    }

    @Test
    void testDeleteTodo() {
        TodoDTO todoDTO = new TodoDTO().setName("Task 1").setDescription("Description 1");
        TodoEntity createdTodo = todoService.create(todoDTO);

        Long id = createdTodo.getId();

        todoService.deleteTask(id);

        assertThrows(TodoNotFoundException.class, () -> todoService.getById(id));
    }

    @Test
    void testUpdateTodo() {
        TodoDTO todoDTO = new TodoDTO().setName("Task 1").setDescription("Description 1");
        TodoEntity createdTodo = todoService.create(todoDTO);

        Long id = createdTodo.getId();

        TodoEntity updatedTodo = todoService.updateEntity(new TodoEntity()
                .setName("Updated Task")
                .setDescription("Updated Description")
                .setStatus(Status.WORK_IN_PROGRESS), id);

        assertEquals("Updated Task", updatedTodo.getName());
        assertEquals("Updated Description", updatedTodo.getDescription());
        assertEquals(Status.WORK_IN_PROGRESS, updatedTodo.getStatus());
    }

    @Test
    void testUpdateStatus_ValidTransition() {
        TodoDTO todoDTO = new TodoDTO().setName("Task 1").setDescription("Description 1");
        TodoEntity createdTodo = todoService.create(todoDTO);

        Long id = createdTodo.getId();

        StatusDTO statusDTO = new StatusDTO();
        statusDTO.setStatus(Status.WORK_IN_PROGRESS.name());

        TodoEntity updatedTodo = todoService.updateStatus(id, statusDTO);

        assertEquals(Status.WORK_IN_PROGRESS, updatedTodo.getStatus());
    }

    @Test
    void testUpdateStatus_InvalidTransition() {
        TodoDTO todoDTO = new TodoDTO().setName("Task 1").setDescription("Description 1");
        TodoEntity createdTodo = todoService.create(todoDTO);

        Long id = createdTodo.getId();

        StatusDTO statusDTO = new StatusDTO();
        statusDTO.setStatus(Status.SIGNED.name());

        assertThrows(TodoBadRequestException.class, () -> todoService.updateStatus(id, statusDTO));
    }

    @Test
     void testDeleteTask_TaskNotFound() {
        Long nonExistingId = 1000L;

        try {
            todoService.deleteTask(nonExistingId);
            fail("Expected TodoNotFoundException to be thrown");
        } catch (TodoNotFoundException e) {
            assertEquals("Cannot find Task by id 1000 for Delete", e.getMessage());
        }
    }

    @Test
     void testUpdateEntity_TaskNotFound() {
        Long nonExistingId = 1000L;
        TodoEntity todoDTO = new TodoEntity();

        try {
            todoService.updateEntity(todoDTO, nonExistingId);
            fail("Expected TodoNotFoundException to be thrown");
        } catch (TodoNotFoundException e) {
            assertEquals("Not exist task by id 1000", e.getMessage());
        }
    }
}
