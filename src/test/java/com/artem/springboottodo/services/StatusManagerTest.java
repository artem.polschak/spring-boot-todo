package com.artem.springboottodo.services;

import static org.junit.jupiter.api.Assertions.*;

import com.artem.springboottodo.models.StatusDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
class StatusManagerTest {

    @Autowired
    private StatusManager statusManager;

    @Test
    void testAllowedTransitions() {
        assertTrue(statusManager.isAllowed(Status.PLANNED, new StatusDTO(Status.WORK_IN_PROGRESS.name())));
        assertTrue(statusManager.isAllowed(Status.PLANNED, new StatusDTO(Status.POSTPONED.name())));
        assertTrue(statusManager.isAllowed(Status.PLANNED, new StatusDTO(Status.CANCELLED.name())));

        assertTrue(statusManager.isAllowed(Status.WORK_IN_PROGRESS, new StatusDTO(Status.NOTIFIED.name())));
        assertTrue(statusManager.isAllowed(Status.WORK_IN_PROGRESS, new StatusDTO(Status.SIGNED.name())));
        assertTrue(statusManager.isAllowed(Status.WORK_IN_PROGRESS, new StatusDTO(Status.CANCELLED.name())));

        assertTrue(statusManager.isAllowed(Status.POSTPONED, new StatusDTO(Status.NOTIFIED.name())));
        assertTrue(statusManager.isAllowed(Status.POSTPONED, new StatusDTO(Status.SIGNED.name())));
        assertTrue(statusManager.isAllowed(Status.POSTPONED, new StatusDTO(Status.CANCELLED.name())));

        assertTrue(statusManager.isAllowed(Status.SIGNED, new StatusDTO(Status.CANCELLED.name())));
        assertTrue(statusManager.isAllowed(Status.SIGNED, new StatusDTO(Status.DONE.name())));

        assertTrue(statusManager.isAllowed(Status.NOTIFIED, new StatusDTO(Status.CANCELLED.name())));
        assertTrue(statusManager.isAllowed(Status.NOTIFIED, new StatusDTO(Status.DONE.name())));
    }

    @Test
    void testNotAllowedTransitions() {
        assertFalse(statusManager.isAllowed(Status.CANCELLED, new StatusDTO(Status.WORK_IN_PROGRESS.name())));
        assertFalse(statusManager.isAllowed(Status.DONE, new StatusDTO(Status.SIGNED.name())));
    }

    @Test
    void testCancelledAndDoneStatus() {
        assertFalse(statusManager.isAllowed(Status.CANCELLED, new StatusDTO(Status.DONE.name())));
        assertFalse(statusManager.isAllowed(Status.DONE, new StatusDTO(Status.CANCELLED.name())));
    }
}
