package com.artem.springboottodo.controllers;

import com.artem.springboottodo.entity.TodoEntity;
import com.artem.springboottodo.models.StatusDTO;
import com.artem.springboottodo.models.TodoDTO;
import com.artem.springboottodo.services.TodoService;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class TodoControllerTest {

    @Test
    void getAllTasks() {
        TodoService todoService = mock(TodoService.class);
        TodoController todoController = new TodoController(todoService);

        List<TodoEntity> todoEntities = new ArrayList<>();
        todoEntities.add(new TodoEntity());
        when(todoService.readAll()).thenReturn(todoEntities);

        ResponseEntity<Iterable<TodoEntity>> response = todoController.getAllTasks();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(todoEntities, response.getBody());
    }

    @Test
    void getTaskById() {
        TodoService todoService = mock(TodoService.class);
        TodoController todoController = new TodoController(todoService);

        Long taskId = 1L;
        TodoEntity todoEntity = new TodoEntity();
        when(todoService.getById(taskId)).thenReturn(todoEntity);

        ResponseEntity<TodoEntity> response = todoController.getTaskById(taskId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(todoEntity, response.getBody());
    }

    @Test
    void createTask() {
        TodoService todoService = mock(TodoService.class);
        TodoController todoController = new TodoController(todoService);

        TodoDTO todoDTO = new TodoDTO();
        TodoEntity createdTodo = new TodoEntity();
        when(todoService.create(todoDTO)).thenReturn(createdTodo);

        ResponseEntity<TodoEntity> response = todoController.createTask(todoDTO);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(createdTodo, response.getBody());
    }

    @Test
    void updateTask() {
        TodoService todoService = mock(TodoService.class);
        TodoController todoController = new TodoController(todoService);

        Long taskId = 1L;
        StatusDTO statusDTO = new StatusDTO();
        TodoEntity updatedTodo = new TodoEntity();
        when(todoService.updateStatus(taskId, statusDTO)).thenReturn(updatedTodo);

        ResponseEntity<TodoEntity> response = todoController.updateTask(taskId, statusDTO);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(updatedTodo, response.getBody());
    }

    @Test
    void deleteTask() {
        TodoService todoService = mock(TodoService.class);
        TodoController todoController = new TodoController(todoService);

        Long taskId = 1L;

        ResponseEntity<Void> response = todoController.deleteTask(taskId);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(todoService, times(1)).deleteTask(taskId);
    }
}
